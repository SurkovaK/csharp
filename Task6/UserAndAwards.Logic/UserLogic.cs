﻿using System.Collections.Generic;
using UserAndAwards.BllContracts;
using UserAndAwards.DalContracts;
using UserAndAwards.Entities;
using UserAndAwards.FileDal;
using UserAndAwards.MemoryDal;

namespace UserAndAwards.Logic
{
    public interface IService
    {
        IDaoForUser Serve();
    }

    public class ServiceFile : IService
    {
        public IDaoForUser Serve()
        {
            return new FileUserDao();
        }
    }

    public class SeviceMemory : IService
    {
        public IDaoForUser Serve()
        {
            return new MemoryUserDao();
        }
    }

    public class UserLogic : ILogicForUser
    {
        private readonly IDaoForUser userDao;

        public UserLogic(IService dao)
        {
            userDao = dao.Serve();
        }

        public int AddUser(User user)
        {
            return userDao.Create(user);
        }
       
        public void Delete(int id)
        {
            userDao.Delete(id);
        }

        public IEnumerable<User> GetAll()
        {
            return userDao.GetAll();
        }

        public void Rewarding(int idOfUser, int idOfAward)
        {
            userDao.Rewarding(idOfUser, idOfAward);
        }

        public void DelAward(int idOfAward)
        {
            userDao.DelAward(idOfAward);
        }
    }
}