﻿using System;
using System.Collections.Generic;
using UserAndAwards.BllContracts;
using UserAndAwards.DalContracts;
using UserAndAwards.Entities;
using UserAndAwards.FileDal;
using UserAndAwards.MemoryDal;

namespace UserAndAwards.Logic
{
    public interface IServiceForAward
    {
        IDaoForAwards Serve();
    }

    public class ServiceFileAward : IServiceForAward
    {
        public IDaoForAwards Serve()
        {
            return new FileAwardDao();
        }
    }

    public class SeviceMemoryAward : IServiceForAward
    {
        public IDaoForAwards Serve()
        {
            return new MemoryAwardDao();
        }
    }

    public class AwardLogic : ILogicForAwards
    {
        private readonly IDaoForAwards awardDao;

        public AwardLogic(IServiceForAward service)
        {
            awardDao = service.Serve();
        }

        public int AddAward(Award award)
        {
            return awardDao.Create(award);
        }

        public void Delete(int id)
        {
            awardDao.Delete(id);
        }

        public IEnumerable<Award> GetAll()
        {
            return awardDao.GetAll();
        }
        public void AddUser(int idOfUser, int idOfAward)
        {
            awardDao.AddUser(idOfUser, idOfAward);
        }

        public void DelUser(int idOfUser)
        {
            awardDao.DelUser(idOfUser);
        }
    }
}