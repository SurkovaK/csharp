﻿using System.Collections.Generic;
using UserAndAwards.Entities;

namespace UserAndAwards.BllContracts
{
    public interface ILogicForUser
    {
        int AddUser(User user);

        void Delete(int id);

        IEnumerable<User> GetAll();

        void Rewarding(int idOfUser, int idOfAward);

        void DelAward(int idOfAward);
    }
}