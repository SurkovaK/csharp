﻿using System.Collections.Generic;
using UserAndAwards.Entities;

namespace UserAndAwards.BllContracts
{
    public interface ILogicForAwards
    {
        int AddAward(Award award);

        void Delete(int id);

        IEnumerable<Award> GetAll();

        void AddUser(int idOfUser, int idOfAward);

        void DelUser(int idOfUser);
    }
}