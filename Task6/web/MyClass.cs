﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserAndAwards.Entities;
using UserAndAwards.Logic;
using UserAndAwards.BllContracts;

namespace WedPart
{
    public static class MyClass
    {
        public static IService service = new SeviceMemory();
        public static UserLogic userLogic = new UserLogic(service);
        public static IServiceForAward service2 = new SeviceMemoryAward();
        public static AwardLogic awardLogic = new AwardLogic(service2);
    }
}