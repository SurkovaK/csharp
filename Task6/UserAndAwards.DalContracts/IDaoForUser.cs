﻿using System.Collections.Generic;
using UserAndAwards.Entities;

namespace UserAndAwards.DalContracts
{
    public interface IDaoForUser
    {
        int Create(User user);

        IEnumerable<User> GetAll();

        void Delete(int id);

        void Rewarding(int idOfUser, int idOfAward);

        void DelAward(int idOfAward);
    }
}