﻿using System.Collections.Generic;
using UserAndAwards.Entities;

namespace UserAndAwards.DalContracts
{
    public interface IDaoForAwards
    {
        int Create(Award award);

        IEnumerable<Award> GetAll();

        void Delete(int id);

        void AddUser(int idOfUser, int idOfAward);

        void DelUser(int idOfUser);
    }
}