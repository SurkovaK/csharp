﻿using System.Collections.Generic;
using System.Linq;
using UserAndAwards.Entities;
using UserAndAwards.DalContracts;

namespace UserAndAwards.MemoryDal
{
    public class MemoryUserDao : IDaoForUser
    {
        private readonly Dictionary<int, User> users;

        public MemoryUserDao()
        {
            users = new Dictionary<int, User>();
        }

        public int Create(User user)
        {
            if (users.Count == 0)
            {
                user.Id = 1;
            }
            else
            {
                user.Id = users.Max(u => u.Key) + 1;
            }

            users.Add(user.Id, user);
            return user.Id;
        }

        public void DelAward(int idOfAward)
        {
            users.Values.All(user => user.Awards.Remove(idOfAward));
        }

        public void Delete(int id)
        {
            users.Remove(id);
        }

        public IEnumerable<User> GetAll()
        {
            return users.Values;
        }

        public void Rewarding(int idOfUser, int idOfAward)
        {
            users[idOfUser].Awards.Add(idOfAward);
        }
    }
}