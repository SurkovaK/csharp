﻿using System.Collections.Generic;
using System.Linq;
using UserAndAwards.DalContracts;
using UserAndAwards.Entities;

namespace UserAndAwards.MemoryDal
{
    public class MemoryAwardDao : IDaoForAwards
    {
        private readonly Dictionary<int, Award> awards;

        public MemoryAwardDao()
        {
            awards = new Dictionary<int, Award>();
        }

        public void AddUser(int idOfUser, int idOfAward)
        {
            awards[idOfAward].Users.Add(idOfUser);
        }

        public int Create(Award award)
        {
            if (awards.Count == 0)
            {
                award.Id = 1;
            }
            else
            {
                award.Id = awards.Max(u => u.Key) + 1;
            }

            awards.Add(award.Id, award);
            return award.Id;
        }

        public void Delete(int id)
        {
            awards.Remove(id);
        }

        public void DelUser(int idOfUser)
        {
            awards.Values.All(award => award.Users.Remove(idOfUser));
        }

        public IEnumerable<Award> GetAll()
        {
            return awards.Values;
        }
    }
}