﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UserAndAwards.DalContracts;
using UserAndAwards.Entities;

namespace UserAndAwards.FileDal
{
    public class FileAwardDao : IDaoForAwards
    {
        private readonly Dictionary<int, Award> awards;
        private readonly string storageFileName;
        private readonly string storageTable;

        public FileAwardDao()
        {
            storageFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Awards.txt");
            using (FileStream file1 = new FileStream(storageFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite)) { }
            storageTable = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Table2.txt");
            using (FileStream file2 = new FileStream(storageTable, FileMode.OpenOrCreate, FileAccess.Read)) { }
            awards = new Dictionary<int, Award>();
            CreateDictionary();
        }

        public void AddUser(int idOfUser, int idOfAward)
        {
            awards[idOfAward].Users.Add(idOfUser);
            UpdateOfStorageTable();
        }

        public int Create(Award award)
        {
            award.Id = awards.Count == 0 ? 1 : awards.Keys.Max() + 1;
            awards.Add(award.Id, award);
            UpdateOfStorageFileName();
            return award.Id;
        }

        public void Delete(int id)
        {
            awards.Remove(id);
            if (awards.Count != 0)
            {
                UpdateOfStorageFileName();
                UpdateOfStorageTable();
            }
            else
            {
                File.WriteAllText(storageFileName, string.Empty, Encoding.Default);
                File.WriteAllText(storageTable, string.Empty, Encoding.Default);
            }
            
        }

        public void DelUser(int idOfUser)
        {
            awards.Values.All(award => award.Users.Remove(idOfUser));
            UpdateOfStorageTable();
        }

        public IEnumerable<Award> GetAll()
        {
            CreateDictionary();
            return awards.Values;
        }

        private void UpdateOfStorageTable()
        {
            File.WriteAllText(storageTable, string.Empty);

            using (StreamWriter writer = new StreamWriter(storageTable, true))
            {
                foreach (var award in awards)
                {
                    writer.Write($"{award.Value.Id} ", Encoding.Default);
                    foreach (var user in award.Value.Users)
                    {
                        writer.Write($"{user} ", Encoding.Default);
                    }

                    writer.WriteLine();
                }

                writer.Close();
            }
        }

        private void UpdateOfStorageFileName()
        {
            File.WriteAllText(storageFileName, string.Empty);
            using (StreamWriter writer = new StreamWriter(storageFileName, true))
            {
                foreach (var award in awards)
                {
                    writer.WriteLine($"{award.Value.Id} {award.Value.Title}", Encoding.Default);
                }

                writer.Close();
            }
        }

        private void CreateDictionary()
        {
            awards.Clear();
            string str = string.Empty;
            using (StreamReader sr = new StreamReader(storageFileName))
            {
                while ((str = sr.ReadLine()) != null)
                {
                    var parts = str.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                    awards.Add(int.Parse(parts[0]), new Award
                    {
                        Id = int.Parse(parts[0]),
                        Title = parts[1],
                    });
                }
            }
            str = string.Empty;
            using (StreamReader sr = new StreamReader(storageTable))
            {
                while ((str = sr.ReadLine()) != null)
                {
                    var parts = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    int k = int.Parse(parts[0]);
                    if (awards.ContainsKey(k))
                    {
                        for (int i = 1; i < parts.Length; i++)
                        {
                            awards[k].Users.Add(int.Parse(parts[i]));
                        }
                    }
                }
            }
        }
    }
}