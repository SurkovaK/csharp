﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UserAndAwards.DalContracts;
using UserAndAwards.Entities;

namespace UserAndAwards.FileDal
{
    public class FileUserDao : IDaoForUser
    {
        private readonly Dictionary<int, User> users;
        private readonly string storageFileName;
        private readonly string storageTable;

        public FileUserDao()
        {
            storageTable = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Table.txt");
            using (FileStream file2 = new FileStream(storageTable, FileMode.OpenOrCreate, FileAccess.Read)) { }
            storageFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Users.txt");
            using (FileStream file1 = new FileStream(storageFileName, FileMode.OpenOrCreate, FileAccess.Read)) { }
            users = new Dictionary<int, User>();
            CreateDictionary();
        }

        public int Create(User user)
        {
            if (users.Count == 0)
            {
                user.Id = 1;
            }
            else
            {
                user.Id = users.Keys.Max() + 1;
            }

            users.Add(user.Id, user);
            UpdateOfStorageFileName();
            return user.Id;
        }

        public void Delete(int id)
        {
            users.Remove(id);
            if (users.Count != 0)
            {
                UpdateOfStorageFileName();
                UpdateOfStorageTable();
            }
            else
            {
                File.WriteAllText(storageFileName, string.Empty, Encoding.Default);
                File.WriteAllText(storageTable, string.Empty, Encoding.Default);
            }
        }

        public IEnumerable<User> GetAll()
        {
            CreateDictionary();
            return users.Values;
        }

        public void DelAward(int idOfAward)
        {
            users.Values.All(user => user.Awards.Remove(idOfAward));
            UpdateOfStorageTable();
        }

        public void Rewarding(int idOfUser, int idOfAward)
        {
            users[idOfUser].Awards.Add(idOfAward);
            UpdateOfStorageTable();
        }

        private void UpdateOfStorageFileName()
        {
            File.WriteAllText(storageFileName, string.Empty);
            using (StreamWriter writer = new StreamWriter(storageFileName, true))
            {
                foreach (var user in users.Values)
                {
                    writer.WriteLine($"{user.Id} {user.Name} {user.SurName} {user.DateOfBirthday.Day}.{user.DateOfBirthday.Month}.{user.DateOfBirthday.Year}", Encoding.Default);
                }

                writer.Close();
            }

        }

        private void UpdateOfStorageTable()
        {
            File.WriteAllText(storageTable, string.Empty);
            using (StreamWriter writer = new StreamWriter(storageTable, true))
            {
                foreach (var user in users)
                {
                    writer.Write($"{user.Value.Id} ", Encoding.Default);
                    foreach (var award in user.Value.Awards)
                    {
                        writer.Write($"{award} ", Encoding.Default);
                    }

                    writer.WriteLine();
                }

                writer.Close();
            }
        }

        private void CreateDictionary()
        {
            users.Clear();
            string str = string.Empty;
            using (StreamReader sr = new StreamReader(storageFileName))
            {
                while ((str = sr.ReadLine()) != null)
                {
                    var parts = str.Split(new[] { ' ' }, 4, StringSplitOptions.RemoveEmptyEntries);
                    users.Add(int.Parse(parts[0]), new User
                    {
                        Id = int.Parse(parts[0]),
                        Name = parts[1],
                        SurName = parts[2],
                        DateOfBirthday = DateTime.Parse(parts[3]),
                    });
                }
            }

            str = string.Empty;
            using (StreamReader sr = new StreamReader(storageTable))
            {
                while ((str = sr.ReadLine()) != null)
                {
                    var parts = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    int k = int.Parse(parts[0]);
                    if (users.ContainsKey(k))
                    {
                        for (int i = 1; i < parts.Length; i++)
                        {
                            users[k].Awards.Add(int.Parse(parts[i]));
                        }
                    }
                }
            }
        }
    }
}
