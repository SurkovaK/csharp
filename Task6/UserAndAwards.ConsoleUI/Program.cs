﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserAndAwards.Entities;
using UserAndAwards.BllContracts;
using UserAndAwards.Logic;
using System.Globalization;

namespace UserAndAwards.ConsoleUI
{
    public class Program
    {
        private static ILogicForUser userLogic;
        private static ILogicForAwards awardLogic;

        private static void Main()
        {
            IService service = new ServiceFile();
            userLogic = new UserLogic(service);
            IServiceForAward service2 = new ServiceFileAward();
            awardLogic = new AwardLogic(service2);
            while (true)
            {
                Console.WriteLine("1-Add user");
                Console.WriteLine("2-Delete user");
                Console.WriteLine("3-Show all users");
                Console.WriteLine("4-Add award");
                Console.WriteLine("5-Delete award");
                Console.WriteLine("6-Show all awards");
                Console.WriteLine("0-Exit");
                string userInput = Console.ReadLine();
                switch (userInput)
                {
                    case "1":
                        RewardingUser(AddUser());
                        break;
                    case "2":
                        DeleteUser();
                        break;
                    case "3":
                        ShowUsers();
                        break;
                    case "4":
                        AddingUserForAward(AddAward());
                        break;
                    case "5":
                        DeleteAward();
                        break;
                    case "6":
                        ShowAwards();
                        break;
                    case "0":
                        return;
                    default:
                        Console.WriteLine("Incorrect input");
                        break;
                }
            }
        }

        private static int AddUser()
        {
            Console.WriteLine("Enter the name of User");
            string inputName = Console.ReadLine();
            Console.WriteLine("Enter the surname of User");
            string inputSurName = Console.ReadLine();
            User user = new User
            {
                Name = inputName,
                SurName = inputSurName,
                DateOfBirthday = GettigDateOfBithday(),
            };
            try
            {
                int idOfUser = userLogic.AddUser(user);
                Console.WriteLine($"User with id:{idOfUser} was added");
                return idOfUser;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot get user\n{ex.Message}");
                return AddUser();
            }
        }

        private static DateTime GettigDateOfBithday()
        {
            string inputDate;
            DateTime dob;
            do
            {
                Console.WriteLine("Enter the date of birthday in format dd.MM.yyyy");
                inputDate = Console.ReadLine();
            }
            while (!DateTime.TryParseExact(inputDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dob));

            return dob;
        }

        private static void RewardingUser(int idOfUser)
        {
            int idOfAward;
            bool flag;
            Console.WriteLine("Do you want to add award of this user?(y/n)");
            string inputAnswer = Console.ReadLine();
            while (inputAnswer == "y")
            {
                do
                {
                    Console.WriteLine("Enter id of this award or create new. (For creating enter 0)");
                    inputAnswer = Console.ReadLine();
                    flag = int.TryParse(inputAnswer, out idOfAward);
                } while (!flag);

                if (awardLogic.GetAll().Any(award => award.Id == idOfAward))
                { 
                    Rewarding(idOfUser, idOfAward);
                }
                else 
                {
                    Console.WriteLine("There is no such ID");
                }

                Console.WriteLine("Do you want to add award of this user?(y/n)");
                inputAnswer = Console.ReadLine();
            }
        }

        private static void Rewarding(int idOfUser, int idOfAward)
        {
            if (idOfAward == 0)
            {
                idOfAward = AddAward();
            }

            if (idOfUser == 0)
            {
                idOfUser = AddUser();
            }

            userLogic.Rewarding(idOfUser, idOfAward);
            awardLogic.AddUser(idOfUser, idOfAward);
        }

        private static void DeleteUser()
        {
            Console.WriteLine("Enter the id for delete");
            int id = Convert.ToInt32(Console.ReadLine());
            userLogic.Delete(id);
            awardLogic.DelUser(id);
            Console.WriteLine($"User with id:{id} was deleted");
        }

        private static void ShowUsers()
        {
            List<User> users = userLogic.GetAll().ToList();
            foreach (var user in users)
            {
                Console.WriteLine($"{user.Id} {user.SurName} {user.Name}  {user.DateOfBirthday.Day}.{user.DateOfBirthday.Month}.{user.DateOfBirthday.Year}");
                foreach (var award in user.Awards)
                {
                    Award myAward = awardLogic.GetAll().First(award1 => award1.Id == award);
                    Console.WriteLine($"\t{myAward.Id}-{myAward.Title}");
                }
            }
        }

        private static int AddAward()
        {
            Console.WriteLine("Enter new award title");
            string inputAward = Console.ReadLine();
            Award award = new Award
            {
                Title = inputAward
            };
            try
            {
                int idOfAward = awardLogic.AddAward(award);
                Console.WriteLine($"Award with id:{idOfAward} was added");
                return idOfAward;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot get new award\n{ex.Message}");
                return AddAward();
            }
        }

        private static void AddingUserForAward(int idOfAward)
        {
            int idOfUser;
            bool flag;
            Console.WriteLine("Do you want to add user of this award?(y/n)");
            string inputAnswer = Console.ReadLine();
            while (inputAnswer == "y")
            {
                do
                {
                    Console.WriteLine("Enter id of this user or create new. (For creating enter 0)");
                    inputAnswer = Console.ReadLine();
                    flag = int.TryParse(inputAnswer, out idOfUser);
                } while (!flag);

                if (userLogic.GetAll().Any(user => user.Id == idOfUser))
                {
                    Rewarding(idOfUser, idOfAward);
                }
                else
                {
                    Console.WriteLine("There is no such ID");
                }

                Console.WriteLine("Do you want to add award of this user?(y/n)");
                inputAnswer = Console.ReadLine();
            }
        }

        private static void ShowAwards()
        {
            List<Award> awards = awardLogic.GetAll().ToList();
            foreach (var award in awards)
            {
                Console.WriteLine($"{award.Id}-{award.Title}");
                foreach (var user in award.Users)
                {
                    User myUser = userLogic.GetAll().First(user1 => user1.Id == user);
                    Console.WriteLine($"\t{myUser.Id}-{myUser.Name}-{myUser.SurName} {myUser.DateOfBirthday.Day}.{myUser.DateOfBirthday.Month}.{myUser.DateOfBirthday.Year}");
                }
            }
        }

        private static void DeleteAward()
        {
            Console.WriteLine("Enter the id of award for delete");
            int id = int.Parse(Console.ReadLine());
            awardLogic.Delete(id);
            userLogic.DelAward(id);
            Console.WriteLine($"Award with id:{id} was deleted");
        }
    }
}