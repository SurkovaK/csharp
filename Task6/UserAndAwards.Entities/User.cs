﻿using System;
using System.Collections.Generic;

namespace UserAndAwards.Entities
{
    public class User
    {
        private string name;
        private string surname;

        public User()
        {
            Awards = new List<int>();
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Name shouldn't be null or empty", nameof(value));
                }
                else if (value.Length > 50 || value.Length < 3)
                {
                    throw new ArgumentException("Invalid name length", nameof(value));
                }
                else
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (char.IsDigit(value[i]))
                        {
                            throw new ArgumentException("Name cannot contains numbers", nameof(value));
                        }
                    }

                    name = value;
                }
            }
        }

        public string SurName
        {
            get
            {
                return surname;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Name shouldn't be null or empty", nameof(value));
                }
                else if (value.Length > 50 || value.Length < 3)
                {
                    throw new ArgumentException("Invalid surname length", nameof(value));
                }
                else
                {
                    for (int i = 0; i < value.Length; i++)
                    {
                        if (char.IsDigit(value[i]))
                        {
                            throw new ArgumentException("Surname contains numbers", nameof(value));
                        }
                    }

                    surname = value;
                }
            }
        }

        public DateTime DateOfBirthday { get; set; }

        public int Id { get; set; }

        public List<int> Awards { get; set; }
    }
}