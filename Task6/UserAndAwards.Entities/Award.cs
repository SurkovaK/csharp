﻿using System;
using System.Collections.Generic;

namespace UserAndAwards.Entities
{
    public class Award
    {
        private string _title;

        public Award()
        {
            Users = new List<int>();
        }

        public int Id { get; set; }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Title of award shouldn't be null or empty");
                }
                else if (value.Length > 50 || value.Length < 5)
                {
                    throw new ArgumentException($"Title cannot have legth={value.Length}");
                }
                else
                {
                    _title = value;
                }
            }
        }

        public List<int> Users { get; set; }
    }
}