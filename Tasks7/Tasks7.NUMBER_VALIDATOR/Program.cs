﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tasks7.NUMBER_VALIDATOR
{
    public class Program
    {
        public static string pattern1 = @"^(\-?\d+([.,]\d+)?)$";
        public static string pattern2 = @"\b[+-]?0*[1-9]([.,]\d+)?(?:E|e)[+-]?[1-9]\d*\b";
        private static void Main()
        {
            while (true)
            {
                Console.WriteLine("Введите число: ");
                string input = Console.ReadLine();
                if (Regex.IsMatch(input, pattern1))
                {
                    Console.WriteLine("Это число в обычной нотации");
                }
                else if (Regex.IsMatch(input, pattern2))
                {
                    Console.WriteLine("Это число в научной нотации");
                }
                else
                {
                    Console.WriteLine("Это не число");
                }
            }
        }
    }
}
