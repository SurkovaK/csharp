using NUnit.Framework;
using System.Text.RegularExpressions;
using Task7.EMAIL_FINDER;

namespace Test7._3.EMAIL_FINDER
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            string myText = "kr150@yand.xh.ru, yourname@cherisheddream.com, marketing.department@mydomain.by, krist@bn";
            var mails = Regex.Matches(myText, Program.pattern);
            Assert.AreEqual(3, mails.Count);
        }

        [Test]
        public void Test2()
        {
            string myText = "mariya.petrova@mydomain.by, vasya.sidorov@mail.ru, user@example.com, ____5fg_@mail.ru";
            var mails = Regex.Matches(myText, Program.pattern);
            Assert.AreEqual(3, mails.Count);
        }

        [Test]
        public void Test3()
        {
            string myText = "mariya.petrova@mydomain.by, vasya.sidorov@mail.ru, user@example.com, MAIhhL@RU";
            var mails = Regex.Matches(myText, Program.pattern);
            Assert.AreEqual(3, mails.Count);
        }
    }
}