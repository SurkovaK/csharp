using NUnit.Framework;
using System;
using System.Text.RegularExpressions;
using Tasks7.HTMLReplacer;

namespace Test7._2.HTML_REPLACER
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            string myText = "<h1>�����������</h1><hr>";
            string q = Regex.Replace(myText, Program.pattern, "_");
            Assert.AreEqual("_�����������__", q);
        }

        [Test]
        public void Test2()
        {
            string myText = "<h1 �����������>";
            string q = Regex.Replace(myText, Program.pattern, "_");
            Assert.AreEqual("<h1 �����������>", q);
        }

        [Test]
        public void Test3()
        {
            string myText = "�����������</h1></h2>";
            string q = Regex.Replace(myText, Program.pattern, "_");
            Assert.AreEqual("�����������__", q);
        }
    }
}