﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tasks7.HTMLReplacer
{
    public class Program
    {
        public static string pattern = @"(<(?:\/?\w+(?:\s*(\w+)\s*=\s*""\w+"")*)>|<(?:\w+\/?)>)";
        private static void Main()
        {
            string text = @"<b class=""buttom""> Это </b> текст <i> с </i> <font color=""red"">HTML</font> кодами";
            string q = Regex.Replace(text, pattern, "_");
            Console.WriteLine(q);
        }
    }
}
