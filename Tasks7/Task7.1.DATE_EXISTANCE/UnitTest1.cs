using NUnit.Framework;
using System.Text.RegularExpressions;
using Tasks7.DateExistance;

namespace Task7._1.DATE_EXISTANCE
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            string myText = "31-02-2152";
            Assert.AreEqual(false, Regex.IsMatch(myText, Program.pattern));
        }

        [Test]
        public void Test2()
        {
            string myText = "30-07-2125";
            Assert.AreEqual(true, Regex.IsMatch(myText, Program.pattern));
        }

        [Test]
        public void Test3()
        {
            string myText = "31-09-2125";
            Assert.AreEqual(false, Regex.IsMatch(myText, Program.pattern));
        }

        [Test]
        public void Test4()
        {
            string myText = "29-02-2020";
            Assert.AreEqual(false, Regex.IsMatch(myText, Program.pattern));
        }
    }
}