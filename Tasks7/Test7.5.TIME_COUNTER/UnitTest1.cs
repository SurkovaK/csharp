using NUnit.Framework;
using System.Text.RegularExpressions;
using Task7.TIME_COUNTER;

namespace Test7._5.TIME_COUNTER
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            string myText = "1:34 10:45 04:31 11:76 12:60 0:56";
            var collection = Regex.Matches(myText, Program.pattern);
            Assert.AreEqual(4, collection.Count);
        }
    }
}