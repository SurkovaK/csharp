using NUnit.Framework;
using System.Text.RegularExpressions;
using Tasks7.NUMBER_VALIDATOR;

namespace Test7._4.NUMBER_VALIDATOR
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            string input = "1.23e-56";
            Assert.AreEqual(true, Regex.IsMatch(input, Program.pattern2));
        }

        [Test]
        public void Test2()
        {
            string input = "1.2356";
            Assert.AreEqual(true, Regex.IsMatch(input, Program.pattern1));
        }

        [Test]
        public void Test3()
        {
            string input = "1.23e56";
            Assert.AreEqual(false, Regex.IsMatch(input, Program.pattern1));
        }

        [Test]
        public void Test4()
        {
            string input = "1,2356";
            Assert.AreEqual(true, Regex.IsMatch(input, Program.pattern1));
        }
        [Test]
        public void Test5()
        {
            string input = "1,2356fg";
            Assert.AreEqual(false, Regex.IsMatch(input, Program.pattern1));
        }

        [Test]
        public void Test6()
        {
            string input = "1,2356E78";
            Assert.AreEqual(true, Regex.IsMatch(input, Program.pattern2));
        }
    }
}