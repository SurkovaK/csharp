﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tasks7.DateExistance
{
    public class Program
    {
        public static string pattern = @"(((?:0[1-9]|[12][0-9]|3[01])-(?:0[13578]|1[02])-(?:\d{4}))|((?:0[1-9]|1[0-9]|2[0-9])-02-\d{4}) )|((?:0[1-9]|[12][0-9]|30)-(?:0[469]|11)-\d{4})";
        private static void Main()
        {
            string text = "31-12-1995";
            Console.WriteLine(Regex.IsMatch(text, pattern) ? "В заданном тексте содержится дата" : "В заданном тексте отсутствуют даты!");
        }
    }
}
