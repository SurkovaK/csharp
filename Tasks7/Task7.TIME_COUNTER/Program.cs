﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task7.TIME_COUNTER
{
    public class Program
    {
        public static string pattern = @"\b(0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]\b";
        private static void Main()
        {
            string text = "В 7:55 я встал, позавтракал и к 10:56 пошёл на работу 00:12 12::12 00::00 1212:00 9:12 33:12 09:12 ";
            var collection = Regex.Matches(text, pattern);
            Console.WriteLine(collection.Count);
        }
    }
}
